const m = require('mithril')
var dateFormat = require('dateformat')

class IssuesList {
    constructor(vnode) {
        this.model = vnode.attrs.model
    }
    oninit() {
        this.model.loadIssues()
    }
    view() {
        return m('table.table', [
            m('thead', [
                m('th', 'title'),
                m('th', 'opened'),
                m('th', 'closed')
            ]),
            m('tbody', [
                this.model.list.map(item =>
                    m('tr', [
                        m('td.title-cell', m("a", {href: `/issues/${item.id}`, oncreate: m.route.link}, item.title)),
                        m('td.opened-cell',dateFormat(item.opened.toLocaleString()),
                            m('td.closed-cell', item.closed))
                    ])
                )
            ])
        ])
    }
}

class ViewIssue {
    constructor(vnode) {
        this.model = vnode.attrs.model
        this.issueId = vnode.attrs.issueId
    }
    oninit() {
        this.model.loadIssue(this.issueId)
    }
    view() {
        let detail = this.model.issues[this.issueId]
        return detail
            ? m('div',[
                    m('.row', [
                        m('h1.col-sm-10', detail.title),
                        m('h1.col-sm-1'), ' Issue ID: ', this.issueId,
                        m('.col-sm-1',
                            m(
                                'a.btn.btn-primary',
                                {href: `/issues/${this.issueId}/edit`, oncreate: m.route.link},
                                'Edit Bug'
                            ),
                            m(
                                'a.btn.btn-primary',
                                {href: `/issues/${this.issueId}/delete`, oncreate: m.route.link},
                                'Delete Bug'
                            )
                        )
                    ]),
                    m('dl.row', [
                        m('dt.col-sm-3', 'Opened'),
                        m('dd.col-sm-3', detail.opened),
                        m('dt.col-sm-3', 'Closed'),
                        m('dd.col-sm-3', detail.closed),
                    ]),
                    m('h2', 'Description'),
                    m('p.description', detail.description)
                ]
            )
            : m('.alert.alert-info', 'The ID you have requested does not exist')
    }
}

class EditIssue {
    constructor(vnode) {
        this.model = vnode.attrs.model
        this.issueId = vnode.attrs.issueId
    }
    async oninit() {
        await this.model.loadIssue(this.issueId)
    }
    view() {
        let issue = this.model.issues[this.issueId]
        return issue
            ? m(IssueEditor, {
                title: issue.title,
                description: issue.description,

                onSubmit: async (fields) => {
                    await this.model.updateIssue(this.issueId, fields)
                    m.route.set(`/issues/${this.issueId}`)
                    m.redraw()
                }
            })
            :m('.alert.alert-info', 'Loading')

    }
}


class DeleteIssue {
    constructor(vnode) {
        this.model = vnode.attrs.model
        this.issueId = vnode.attrs.issueId
    }
    async oninit() {
        await this.model.loadIssue(this.issueId)
    }
    view() {
        let issue = this.model.issues[this.issueId]
        return issue
            ? m(DeleteIssueEditor, {
                title: issue.title,
                description: issue.description,

                onSubmit: async (fields) => {
                    await this.model.deleteIssue(this.issueId,fields)
                    m.route.set(`/issues/${this.issueId}`)
                    m.redraw()
                }
            })
            :m('.alert.alert-info', 'The ID you have requested to delete does not exist' , )

    }
}

class CreateIssue {
    constructor(vnode) {
        this.model = vnode.attrs.model
    }
    view() {
        return m(IssueEditor, {
            title: '',
            description: '',
            onSubmit: async ({description, title}) => {
                await this.model.createIssue({description: description, title: title})
                m.route.set(`/issues`)
                m.redraw()
            }
        })
    }
}



class IssueEditor {
    constructor(vnode) {
        this.title = vnode.attrs.title
        this.description = vnode.attrs.description
        this.onSubmit = vnode.attrs.onSubmit
    }
    view() {
        return m('form', {onsubmit: e => this.onSubmit({title: this.title, description: this.description})}, [
            m('.form-group', [
                m('label', {'for': 'title-input'}, 'Issue Title'),
                m('input.form-control#title-input', {value: this.title, oninput: (e) => {this.title = e.target.value}})
            ]),
            m('.form-group', [
                m('label', {'for': 'description-input'}, 'Description'),
                m('textarea.form-control#description-input', {oninput: (e) => {this.description = e.target.value}}, this.description)
            ]),
            m('button.btn.btn-primary#save-button', {type: 'submit'}, 'Save')

        ])
    }
}

class DeleteIssueEditor {
    constructor(vnode) {
        this.title = vnode.attrs.title
        this.description = vnode.attrs.description
        this.onSubmit = vnode.attrs.onSubmit
    }
    view() {
        return m('form', {onsubmit: e => this.onSubmit({title: this.title, description: this.description})}, [
            m('.form-group', [
                m('label',  'The following will be deleted '),
                m('', ),
                m('label', {'for': 'title-input'}, 'Issue Title'),
                m('input.form-control#title-input', {value: this.title, oninput: (e) => {this.title = e.target.value}})
            ]),
            m('.form-group', [
                m('label', {'for': 'description-input'}, 'Description'),
                m('textarea.form-control#description-input', {oninput: (e) => {this.description = e.target.value}}, this.description)
            ]),
            m('button.btn.btn-primary#save-button', {type: 'submit'}, 'Delete')

        ])
    }
}



const ToolbarContainer = {
    view(vnode) {
        return m('div', [
            m('nav.navbar.navbar-expand-lg.navbar-light.bg-light', [
                m('a.navbar-brand', {href: '/issues', oncreate: m.route.link}, 'Bug Tracker'),
                m('.collapse.navbar-collapse', [
                    m('ul.navbar-nav', [
                        m('li.nav-item', [
                            m('a.nav-link', {href: '/issues/create', oncreate: m.route.link}, 'Create')
                        ])
                    ])
                ])
            ]),
            m('.container', vnode.children)
        ])
    }
}

module.exports = {IssuesList, ViewIssue, EditIssue, DeleteIssue, DeleteIssueEditor, CreateIssue, IssueEditor, ToolbarContainer}